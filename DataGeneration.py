import sys
import random


class Item:
    w = -1
    h = -1
    id = -1

    def __init__(self, w, h):
        self.h = h
        self.w = w

    def set_id(self, id_):
        self.id = id_


def set_ids(items):
    for i in range(len(items)):
        items[i].set_id(i)


def save_instance(instance_items, instance_width, instance_name, file_path):
    fp = open(file_path, "w")
    fp.write("N " + instance_name + "\n")
    fp.write("D 2\n")
    fp.write("W " + str(instance_width) + "\n")
    for item in instance_items:
        fp.write(str(item.id) + " " + str(item.w) + " " + str(item.h) + "\n")
    fp.write("EOF")
    print("DONE!")
    fp.close()


def data_gen(n, width, height):
    items = [Item(float(width), float(height))]  # initial item is size specified
    while True:
        r = items[random.randint(0, len(items) - 1)]
        items.remove(r)

        cutting_orientation = random.randint(0, 1)
        i = Item(5, 10)
        if cutting_orientation == 1:  # Horizontal
            cut_pos = random.uniform(0, r.h)
            items.append(Item(r.w, cut_pos))
            items.append(Item(r.w, r.h - cut_pos))
        else:  # Vertical
            cut_pos = random.uniform(0, r.w)
            items.append(Item(cut_pos, r.h))
            items.append(Item(r.w - cut_pos, r.h))
        print("Items len: " + str(len(items)))

        if len(items) == n:
            break

    set_ids(items)
    return items


def data_gen_aspect_ratio(n, width, height, p):
    assert p >= 2
    assert 2 * height / p <= width <= p * height / 2

    items = [Item(float(width), float(height))]  # initial item is size specified

    while len(items) != int(n):
        r = items[random.randint(0, len(items) - 1)]
        items.remove(r)

        cutting_orientation = random.randint(0, 1)

        if cutting_orientation == 1:  # Horizontal
            if 2 * r.w / p <= r.h <= 2 * p * r.w:  # satisfies the horizontal cut constraint
                cut_pos = random.uniform(max(r.w / p, r.h - r.w * p), min(r.w * p, r.h - r.w / p))
                items.append(Item(r.w, cut_pos))
                items.append(Item(r.w, r.h - cut_pos))
            else:  # if horizontal constraint fails then a vertical cut is implied
                cut_pos = random.uniform(max(r.h / p, r.w - r.h * p), min(r.h * p, r.w - r.h / p))
                items.append(Item(cut_pos, r.h))
                items.append(Item(r.w - cut_pos, r.h))
        else:  # Vertical
            if 2 * r.h / p <= r.w <= 2 * p * r.h:  # satisfies the vertical cut constraint
                cut_pos = random.uniform(max(r.h / p, r.w - r.h * p), min(r.h * p, r.w - r.h / p))
                items.append(Item(cut_pos, r.h))
                items.append(Item(r.w - cut_pos, r.h))
            else:  # if vertical constraint fails then a horizontal cut is implied
                cut_pos = random.uniform(max(r.w / p, r.h - r.w * p), min(r.w * p, r.h - r.w / p))
                items.append(Item(r.w, cut_pos))
                items.append(Item(r.w, r.h - cut_pos))

    set_ids(items)
    return items


def get_feasible_set(items, current_max, y):
    ret = []
    a = area(current_max)
    for item in items:
        if area(item) > 2 * a / y:
            ret.append(item)
    return ret


def area(item):
    return item.w * item.h


def data_gen_area_ratio(n, width, height, y):
    assert y >= 2

    items = [Item(float(width), float(height))]  # initial item is size specified

    while len(items) != n:
        current_max = items[0]
        for item in items:
            if area(item) > area(current_max):
                current_max = item

        feasible_set = get_feasible_set(items, current_max, y)

        chosen_item = feasible_set[random.randint(0, len(feasible_set) - 1)]
        items.remove(chosen_item)

        cutting_orientation = random.randint(0, 1)

        if cutting_orientation == 1:
            cut_pos = random.uniform(area(current_max) / (y * chosen_item.w), chosen_item.h / 2)
            items.append(Item(chosen_item.w, cut_pos))
            items.append(Item(chosen_item.w, chosen_item.h - cut_pos))
        else:
            cut_pos = random.uniform(area(current_max) / (y * chosen_item.h), chosen_item.w / 2)
            items.append(Item(cut_pos, chosen_item.h))
            items.append(Item(chosen_item.w - cut_pos, chosen_item.h))

    set_ids(items)
    return items


def data_gen_aspect_and_area(n, width, height, p, y):
    assert p >= 2
    assert y >= 2
    assert 2 * height / p <= width <= p * height / 2

    items = [Item(float(width), float(height))]  # initial item is size specified

    while len(items) != int(n):
        current_max = items[0]
        for item in items:
            if area(item) > area(current_max):
                current_max = item

        feasible_set = get_feasible_set(items, current_max, y)

        r = feasible_set[random.randint(0, len(feasible_set) - 1)]
        items.remove(r)

        m = area(current_max)

        cutting_orientation = random.randint(0, 1)

        if cutting_orientation == 1:  # Horizontal
            if 2 * r.w / p <= r.h <= 2 * p * r.w:  # satisfies the horizontal cut constraint
                cut_pos = random.uniform(max(r.w / p, r.h - r.w * p, m / (y * r.w)),
                                         min(r.w * p, r.h - r.w / p, r.h / 2))
                items.append(Item(r.w, cut_pos))
                items.append(Item(r.w, r.h - cut_pos))
            else:  # if horizontal constraint fails then a vertical cut is implied
                cut_pos = random.uniform(max(r.h / p, r.w - r.h * p, m / (y * r.h)),
                                         min(r.h * p, r.w - r.h / p, r.w / 2))
                items.append(Item(cut_pos, r.h))
                items.append(Item(r.w - cut_pos, r.h))
        else:  # Vertical
            if 2 * r.h / p <= r.w <= 2 * p * r.h:  # satisfies the vertical cut constraint
                cut_pos = random.uniform(max(r.h / p, r.w - r.h * p, m / (y * r.h)),
                                         min(r.h * p, r.w - r.h / p, r.w / 2))
                items.append(Item(cut_pos, r.h))
                items.append(Item(r.w - cut_pos, r.h))
            else:  # if vertical constraint fails then a horizontal cut is implied
                cut_pos = random.uniform(max(r.w / p, r.h - r.w * p, m / (y * r.w)),
                                         min(r.w * p, r.h - r.w / p, r.h / 2))
                items.append(Item(r.w, cut_pos))
                items.append(Item(r.w, r.h - cut_pos))

    set_ids(items)

    return items


if __name__ == '__main__':

    if sys.argv[-1] == "basic":
        save_instance(
            data_gen(
                int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3])
            ),
            sys.argv[2],
            sys.argv[4],
            sys.argv[5]
        )

    elif sys.argv[-1] == "aspect":
        save_instance(
            data_gen_aspect_ratio(
                int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4])
            ),
            sys.argv[2],
            sys.argv[5],
            sys.argv[6]
        )

    elif sys.argv[-1] == "area":
        save_instance(
            data_gen_area_ratio(
                int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4])
            ),
            sys.argv[2],
            sys.argv[5],
            sys.argv[6]
        )

    elif sys.argv[-1] == "aspect-area":
        save_instance(
            data_gen_aspect_and_area(
                int(sys.argv[1]), float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]), float(sys.argv[5])
            ),
            sys.argv[2],
            sys.argv[6],
            sys.argv[7]
        )

    else:
        print("Incorrect final parameter")
